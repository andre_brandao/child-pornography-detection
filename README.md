# Child Pornography Detection

This software combines classical computer vision and deep learning algorithms to detect child pornography images.

## Requirements

This software makes use of the following packages:

* Numpy.
* Matplotlib.
* Opencv.
* Caffe.
* Pandas.
* Scikit-Learn.

## Usage

In order to get the classification for an image img.jpg 

```
python3 main.py img.jpg
```

## Modules

### Pornography Detection

The module for pornography detection is in the *skin_detector.py* file and can be used the same way as the main file.

### Age Detection

The module for the age detection is in the *age_classifier.py* file and can be used the same way as the main file.
